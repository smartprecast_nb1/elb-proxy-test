var net = require('net');
var request = require("request");

const lambdaUrl = "https://mbspwqlkq0.execute-api.eu-west-1.amazonaws.com/prod";


export function start() {
    // Create and return a net.Server object, the function will be invoked when client connect to this server.
    var server = net.createServer( function(client) {

        console.log('Client connect. Client local address : ' + client.localAddress + ':' + client.localPort + '. client remote address : ' + client.remoteAddress + ':' + client.remotePort);

        client.setEncoding('utf-8');

        client.setTimeout(1000);

        // When receive client data.
        client.on('data', function (data) {

            // Print received client data and length.
            // console.log('Receive client send data : ' + data + ', data size : ' + client.bytesRead);
            // elabor(data);
            executePost(data)
            .then(function(retval){
                console.log("DEBUG==>retval", retval);
            });

            // Server send data back to client use client net.Socket object.
            client.end('Server received data : ' + data + ', send back to client data size : ' + client.bytesWritten);
        });

        // When client send data complete.
        client.on('end', function () {
            console.log('Client disconnect.');

            // Get current connections count.
            server.getConnections(function (err, count) {
                if(!err)
                {
                    // Print current connection count in server console.
                    console.log("There are %d connections now. ", count);
                }else
                {
                    console.error(JSON.stringify(err));
                }

            });
        });

        // When client timeout.
        client.on('timeout', function () {
            console.log('Client request time out. ');
        })
    });

    // Make the server a TCP server listening on port 9999.
    server.listen(6027, function () {

        // Get server address info.
        var serverInfo = server.address();

        var serverInfoJson = JSON.stringify(serverInfo);

        console.log('TCP server listen on address : ' + serverInfoJson);

        server.on('close', function () {
            console.log('TCP server socket is closed.');
        });

        server.on('error', function (error) {
            console.error(JSON.stringify(error));
        });

    });
}

async function elabor(data) {
    var response = {
        "retval": true,
        "dataObject": data,
        "message": "done"
    }
    console.log("DEBUG==>response", response);
}

export async function executePost (data) {
    return new Promise(function (resolve, reject) {
        request.post({
            headers : {
                "Content-Type"  : "application/json"
            },
            uri: lambdaUrl,
            json: {
                "message": data
            }
        }, function (error, response, body) {
            if (!error) {   
                resolve({
                    "retval": true,
                    "dataObject": response.body,
                    "message": "done"
                });
            }
            else {     
                resolve({
                    "retval": false,
                    "dataObject": "",
                    "message": error
                });
            }
        }); 
    });   
}