var request = require("request");

const lambdaUrl = "https://mbspwqlkq0.execute-api.eu-west-1.amazonaws.com/prod";

function executePost1 (message, url, success, fail) {
    return new Promise(function (resolve, reject) {
        request.post({
            headers : {
                "Content-Type"  : "application/json"
            },
            uri: url,
            json: message
        }, function (error, response, body) {
            if (error) {   
                resolve(fail);
            }
            else {     
                resolve(success);
            }
        }); 
    });   
}

export async function executePost (message) {
    return new Promise(function (resolve, reject) {
        request.post({
            headers : {
                "Content-Type"  : "application/json"
            },
            uri: lambdaUrl,
            json: message
        }, function (error, response, body) {
            if (!error) {   
                resolve({
                    "retval": true,
                    "dataObject": response.body,
                    "message": "done"
                });
            }
            else {     
                resolve({
                    "retval": false,
                    "dataObject": "",
                    "message": error
                });
            }
        }); 
    });   
}

log_receive_url = 'https://trb5n0u7ej.execute-api.eu-west-1.amazonaws.com/debug/log-receive';

exports.executePost1 = executePost1;