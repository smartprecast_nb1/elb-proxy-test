"use strict";
import "babel-polyfill";

import express, { Router } from 'express';
import { json, urlencoded } from 'body-parser';
import cors from 'cors';
const path = require('path'); 


import * as sample1 from "./lib/sample1";
import * as tcpServer from "./lib/tcpServer"


var port = process.env.PORT || 1377;

const app = express();
const router = express.Router();

router.use(cors());
router.use('/apiDoc', express.static(path.join(__dirname, 'apiDoc/')));

router.use(json());
router.use(urlencoded({ extended: true }));

router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "false");
    res.header("Access-Control-Allow-Methods", "POST,GET,OPTIONS,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,  x-access-token");
    next();
});

/**
 @apiDefine DefaultSuccessfulResponseSave
 @apiSuccess {Object} JSON element.

 */
/**
 @apiDefine DefaultSuccessfulResponseText
 @apiSuccess {text} result object.
 */

router.get("/", (req, res, next) => {
    res.status(200);
    res.send("uh? it seems there is another Odyss service, here!");
});


/** 
@api {post} /sample1   sample1 post 
@apiVersion 0.1.0
@apiName sample1 post 
@apiGroup 1 samples
@apiDescription sample1 post.
@apiParam {Object} [value]    any json value.
@apiUse DefaultSuccessfulResponseSave
@apiExample Usage: @api {post} /sample1
body: 
{
...
}
@apiSuccessExample Success-Response:
{
...
}
@apiSampleRequest /sample1
*/
router.post("/sample1", async (req, res, next) => {
    var response = await sample1.executePost(req.body);
    
    res.json(response.dataObject);
});

router.post("/log_receive", async (req, res, next) => {
    var response = await sample1.executePost1(req.body, 'url', 'success', 'fail');
    
    res.json(response);
});


app.use('/', router);

tcpServer.start();

app.listen(port);
console.log("server run on port:", port);