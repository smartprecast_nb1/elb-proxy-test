# elb-proxy-test


## Installation
Make sure you have the [aws cli](https://aws.amazon.com/cli/?nc1=h_ls) installed. 

## Istance creation

build the application
```
npm install
npm install apidoc -g   
npm run build
```

### step 1
```
eb init
```
- select a default region: 4 (eu-west-1)
- select an application to use: 2 [ Create new Application ]
- Enter Application Name: in this example "elb-proxy-test"
- It appears you are using Node.js. Is this correct?: Y
- Select a platform branch: 1 = Node.js 12 running on 64bit Amazon Linux 2
- Do you wish to continue with CodeCommit? (y/N) (default is n): N
- Do you want to set up SSH for your instances? : N


### step 2
```
eb create
```
- Enter Environment Name: in this sample is: elb-proxy-test
- Enter DNS CNAME prefix: leave the default value or enter Environment name
- Select a load balancer type: 1 (Classic)
- Would you like to enable Spot Fleet requests for this environment?: N

at this point elastic beanstalk is created. wait for completion

if you connect to the AWS console under the heading: Elastic Beanstalk, you can see the environment being created

![1](https://test-solarfast.s3-eu-west-1.amazonaws.com/1.png)

### step 3
#### configuration of the instance to enable TCP port 6027
- from the console, select the ec2 service
![2](https://test-solarfast.s3-eu-west-1.amazonaws.com/2.png)
- select "running instances" or "instanze in esecuzione" (this depends on the language of the portal)
![3](https://test-solarfast.s3-eu-west-1.amazonaws.com/3.png)
- select the instance. in this case: "elb-proxy" and then in the area below: "security" (sicurezza)
to configure the ports click on the link: "sg-00a3b3fb98b49865f (awseb-e-pmxkiemkyi-stack-AWSEBSecurityGroup-SDO3CRVOELPB)"
immediately below: "Security groups" (Gruppi di sicurezza)
![4](https://test-solarfast.s3-eu-west-1.amazonaws.com/4.png)
- create a new TCP rule for port 6027
![5](https://test-solarfast.s3-eu-west-1.amazonaws.com/5.png)

### step 4
#### load balancing configuration for elastic beanstalk to enable the listening port
- select Elastic Beanstalk from the console
![6](https://test-solarfast.s3-eu-west-1.amazonaws.com/6.png)
- select the environment. in this case: elb-proxy
![7](https://test-solarfast.s3-eu-west-1.amazonaws.com/7.png)
- now choose "Configuration" (Connfigurazione)
![8](https://test-solarfast.s3-eu-west-1.amazonaws.com/8.png)
- click on "Edit" in the section: "Load balancer"
![9](https://test-solarfast.s3-eu-west-1.amazonaws.com/9.png)
- select: "Add Listener" (Aggiungi listener)
![10](https://test-solarfast.s3-eu-west-1.amazonaws.com/10.png)
- create the rule
![11](https://test-solarfast.s3-eu-west-1.amazonaws.com/11.png)
- Apply the rules
![12](https://test-solarfast.s3-eu-west-1.amazonaws.com/12.png)

### step 5
### deploy manually
```
eb deploy
```